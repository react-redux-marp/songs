import { combineReducers} from 'redux';

const songsReducer = () => {
    return [
        { title: 'Song1', duration: '5:00' },
        { title: 'Song2', duration: '5:01' },
        { title: 'Song3', duration: '5:02' },
        { title: 'Song4', duration: '5:03' },
        { title: 'Song5', duration: '5:04' }
    ];
};

const selectedSongReducer = (selectedSong = null, action) => {
    if(action.type == 'SONG_SELECTED'){
        return action.payload;
    }
    return selectedSong;
};

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});