import React from 'react';
import SongList from './song-list';
import SongDetail from './song-detail';

const App = () => {
  return (<div className="container">
    <div className="row">
      <div className="col-12 col-md-6">
        <SongList></SongList>
      </div>
      <div className="col-12 col-md-6">
        <SongDetail></SongDetail>
      </div>
    </div>
  </div>);
};

export default App;