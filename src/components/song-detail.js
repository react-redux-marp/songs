import React from 'react';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        currentSong: state.selectedSong || {title:'No song selected',duration:'...'}
    };
};

const SongDetail = (props) => {
    return (
        <div>{props.currentSong.title} - {props.currentSong.duration}</div>
    );
};

export default connect(mapStateToProps)(SongDetail);